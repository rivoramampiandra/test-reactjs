import { Component, ChangeEvent } from 'react';
import { RouteComponentProps } from 'react-router-dom';

import UserDataService from '../services/user.service';
import IUserData from "../types/user.type";

interface RouterProps {
    id: string
}

type Props = RouteComponentProps<RouterProps>;

type State = {
    currentUser: IUserData,
    message: string;
}

export default class User extends Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.onChangeFirstName = this.onChangeFirstName.bind(this);
        this.onChangeLastName = this.onChangeLastName.bind(this);
        this.onChangeSexe = this.onChangeSexe.bind(this);
        this.getUser = this.getUser.bind(this);
        this.updateUser = this.updateUser.bind(this);
        this.deleteUser = this.deleteUser.bind(this);

        this.state = {
            currentUser: {
                id: null,
                firstname: '',
                lastname: '',
                sexe: ''
            },
            message: '',
        };
    }

    componentDidMount() {
        this.getUser(this.props.match.params.id);
    }

    onChangeFirstName(e: ChangeEvent<HTMLInputElement>) {
        const firstname = e.target.value;

        this.setState(function (prevState) {
            return {
                currentUser: {
                    ...prevState.currentUser,
                    firstname: firstname,
                },
            };
        });
    }

    onChangeLastName(e: ChangeEvent<HTMLInputElement>) {
        const lastname = e.target.value;

        this.setState(function (prevState) {
            return {
                currentUser: {
                    ...prevState.currentUser,
                    lastname: lastname,
                },
            };
        });
    }

    onChangeSexe(e: ChangeEvent<HTMLInputElement>) {
        const sexe = e.target.value;

        this.setState(function (prevState) {
            return {
                currentUser: {
                    ...prevState.currentUser,
                    sexe: sexe,
                },
            };
        });
    }

    getUser(id: string) {
        UserDataService.get(id)
            .then((response) => {
                this.setState({
                    currentUser: response.data,
                });
                console.log(response.data);
            })
            .catch((e) => {
                console.log(e);
            });
    }

    updateUser() {
        UserDataService.update(
            this.state.currentUser,
            this.state.currentUser.id
        )
            .then((response) => {
                console.log(response.data);
                this.setState({
                    message: "The user was updated successfully!",
                });
            })
            .catch((e) => {
                console.log(e);
            });
    }

    deleteUser() {
        UserDataService.delete(this.state.currentUser.id)
            .then((response) => {
                console.log(response.data);
                this.props.history.push("/users");
            })
            .catch((e) => {
                console.log(e);
            });
    }

    render() {
        const { currentUser } = this.state;

        return (
            <>
                {currentUser ? (
                    <div className="edit-form">
                        <h4>User</h4>
                        <form>
                            <div className="form-group">
                                <label htmlFor="firstname">Firstname</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="firstname"
                                    value={currentUser.firstname}
                                    onChange={this.onChangeFirstName}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="lastname">Lastname</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="lastname"
                                    value={currentUser.lastname}
                                    onChange={this.onChangeLastName}
                                />
                            </div>

                            <div className="form-group">
                                <label htmlFor="sexe">Sexe</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="sexe"
                                    value={currentUser.sexe}
                                    onChange={this.onChangeSexe}
                                />
                            </div>
                        </form>

                        <button
                            className="badge badge-danger mr-2"
                            onClick={this.deleteUser}
                        >
                            Delete
                        </button>

                        <button
                            type="submit"
                            className="badge badge-success"
                            onClick={this.updateUser}
                        >
                            Update
                        </button>
                        <p>{this.state.message}</p>
                    </div>
                ) : (
                    <div>
                        <br />
                        <p>Please click on an User...</p>
                    </div>
                )}
            </>
        )
    }

}