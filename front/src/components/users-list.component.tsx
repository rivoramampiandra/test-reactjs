import { Component, ChangeEvent } from 'react';
import UserDataService from '../services/user.service';
import { Link } from "react-router-dom";
import IUserData from "../types/user.type";

type Props = {};

type State = {
  users: Array<IUserData>,
  currentUser: IUserData | null,
  currentIndex: number,
  searchName: string
};

export default class UserList extends Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.onChangeSearchName = this.onChangeSearchName.bind(this);
        this.findAllUsers = this.findAllUsers.bind(this);
        this.refreshList = this.refreshList.bind(this);
        this.setActiveUser = this.setActiveUser.bind(this);
        this.removeAllUsers = this.removeAllUsers.bind(this);
        this.searchName = this.searchName.bind(this);

        this.state = {
            users: [],
            currentUser: null,
            currentIndex: 1,
            searchName: ''
        };
    }

    componentDidMount() {
        this.findAllUsers();
    }

    onChangeSearchName(e: ChangeEvent<HTMLInputElement>) {
        const searchName = e.target.value;

        this.setState({
            searchName: searchName
        });
    }

    findAllUsers() {
        UserDataService.getAll()
            .then(response => {
                this.setState({
                    users: response.data
                });
                console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    }

    refreshList() {
        this.findAllUsers();
        this.setState({
            currentUser: null,
            currentIndex: -1
        });
    }

    setActiveUser(user: IUserData, index: number) {
        this.setState({
            currentUser: user,
            currentIndex: index
        });
    }

    removeAllUsers() {
        UserDataService.deleteAll()
            .then(response => {
                console.log(response.data);
                this.refreshList();
            })
            .catch(e => {
                console.log(e);
            });
    }

    searchName() {
        this.setState({
            currentUser: null,
            currentIndex: -1
        });

        UserDataService.findByTitle(this.state.searchName)
            .then(response => {
                this.setState({
                    users: response.data
                });
                console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    }

    render() {
        const { searchName, users, currentUser, currentIndex } = this.state;
        return(
            <>
                <div className="list row">
                    <div className="col-md-8">
                        <div className="input-group mb-3">
                            <input type="text" className="form-control" placeholder='search by name' value={searchName} onChange={this.onChangeSearchName}/>
                        </div>
                        <div className="input-group-append">
                            <button className="btn btn-outline-secondary" type='button' onClick={this.searchName}>
                                Search
                            </button>
                        </div>
                    </div>
                </div>
                <div className="col-md-6">
                    <h4>Users List</h4>

                    <ul className="list-group">
                        {users &&
                        users.map((user: IUserData, index: number) => (
                            <li
                                className={
                                    "list-group-item " +
                                    (index === currentIndex ? "active" : "")
                                }
                                onClick={() => this.setActiveUser(user, index)}
                                key={index}
                            >
                                {user.firstname}
                            </li>
                        ))}
                    </ul>

                    <button
                        className="m-3 btn btn-sm btn-danger"
                        onClick={this.removeAllUsers}
                    >
                        Remove All
                    </button>
                </div>
                <div className="col-md-6">
                    {currentUser ? (
                        <div>
                            <h4>User</h4>
                            <div>
                                <label>
                                    <strong>Firstname:</strong>
                                </label>{" "}
                                {currentUser.firstname}
                            </div>
                            <div>
                                <label>
                                    <strong>Lastname:</strong>
                                </label>{" "}
                                {currentUser.lastname}
                            </div>
                            <div>
                                <label>
                                    <strong>Sexe:</strong>
                                </label>{" "}
                                {currentUser.sexe}
                            </div>

                            <Link
                                to={"/users/" + currentUser.id}
                                className="badge badge-warning"
                            >
                                Edit
                            </Link>
                        </div>
                    ) : (
                        <div>
                            <br />
                            <p>Please click on an User...</p>
                        </div>
                    )}
                </div>
            </>
        );
    }
}