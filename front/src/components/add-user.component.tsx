import { Component, ChangeEvent } from 'react';
import UserDataService from '../services/user.service';
import IUserData from "../types/user.type";

type Props = {};
type State = IUserData & {
    submitted : boolean
};

export default class AddUser extends Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.onChangeFirstName = this.onChangeFirstName.bind(this);
        this.onChangeLastName = this.onChangeLastName.bind(this);
        this.onChangeSexe = this.onChangeSexe.bind(this);
        this.saveUser = this.saveUser.bind(this);
        this.newUser = this.newUser.bind(this);

        this.state = {
            id : null,
            firstname : '',
            lastname : '',
            sexe : '',
            submitted : false
        };
    }

    onChangeFirstName(e: ChangeEvent<HTMLInputElement>) {
        this.setState({
            firstname : e.target.value
        });
    }

    onChangeLastName(e: ChangeEvent<HTMLInputElement>) {
        this.setState({
            lastname : e.target.value
        });
    }

    onChangeSexe(e: ChangeEvent<HTMLInputElement>) {
        this.setState({
            sexe : e.target.value
        });
    }

    saveUser() {
        const data: IUserData = {
            firstname : this.state.firstname,
            lastname : this.state.lastname,
            sexe : this.state.sexe
        }

        UserDataService.create(data)
            .then(response => {
                this.setState({
                    id: response.data.id,
                    firstname: response.data.firstname,
                    lastname: response.data.lastname,
                    sexe: response.data.sexe,
                    submitted: true
                });
                console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    }

    newUser() {
        this.setState({
            id: null,
            firstname: '',
            lastname: '',
            sexe: '',
            submitted: false
        });
    }

    render() {
        const { submitted, firstname, lastname, sexe } = this.state;

        return (
          <>
              <div className="submit-form">
                  {submitted ? (
                      <div>
                          <h4>You submitted successfully !</h4>
                          <button className="btn btn-success" onClick={this.newUser}>
                              Add
                          </button>
                      </div>
                  ) : (
                      <div>
                          <div className="form-group">
                              <label htmlFor="firstname">Firstname</label>
                              <input type="text" name="firstname" id="firstname" className="form-control"
                                     required value={firstname} onChange={this.onChangeFirstName}/>
                          </div>
                          <div className="form-group">
                              <label htmlFor="lastname">Lastname</label>
                              <input type="text" name="lastname" id="lastname" className="form-control"
                                     required value={lastname} onChange={this.onChangeLastName}/>
                          </div>
                          <div className="form-group">
                              <label htmlFor="sexe">Sexe</label>
                              <input type="text" name="sexe" id="sexe" className="form-control"
                                     required value={sexe} onChange={this.onChangeSexe}/>
                          </div>

                          <button onClick={this.saveUser} className="btn btn-success">
                              Submit
                          </button>
                      </div>
                  )}
              </div>
          </>
        );
    }
}