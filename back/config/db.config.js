module.exports = {
    HOST : 'localhost',
    USER : 'rivo',
    PASSWORD : 'password',
    DB : 'reactjs',
    dialect : 'mysql',
    pool : {
        max : 5,
        min : 0,
        acquire : 30000,
        idle : 10000
    }
};