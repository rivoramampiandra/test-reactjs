const db = require('../models');
const User = db.users;
const Op = db.Sequelize.Op;

// Create
exports.create = (req, res) => {
    // Validator
    if (!req.body.firstname) {
        res.status(400).send({
            message: 'Firstname cannot be empty'
        });
        return;
    } else if (!req.body.lastname) {
        res.status(400).send({
            message: 'Lastname cannot be empty'
        });
        return;
    }

    // Create an user
    const user = {
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        sexe: req.body.sexe
    };

    // Save to database
    User.create(user)
        .then(data => {
            res.send(data);
        })
        .catch(e => {
            res.status(500).send({
                message: e.message || 'Erreur durant la création de l\'utilisateur'
            });
        });
};

// Find All Users from database
exports.findAll = (req, res) => {
    const firstname = req.query.firstname;
    var condition = firstname ? { firstname : { [Op.like] : `%${firstname}%` } } : null;

    User.findAll({ where: condition })
        .then(data => {
            res.send(data);
        })
        .catch(e => {
            res.status(500).send({
                message: e.message || 'Erreur lors de la récupération de tous les utilisateurs'
            });
        });
};

// Find One User from data
exports.findOne = (req, res) => {
    const id = req.params.id;

    User.findByPk(id)
        .then(data => {
            res.send(data);
        })
        .catch(e => {
            res.status(500).send({
                message: e.message || 'Erreur lors de la récupération de l\'utilisateur'
            })
        })
};

// Update
exports.update = (req, res) => {
    const id = req.params.id;

    User.update(req.body, {
        where: { id: id }
    })
        .then(num => {
            if (num === 1) {
                res.send({
                    message: 'Utilisateur mis à jour avec succès'
                });
            } else {
                res.send({
                    message: `Peut pas mettre à jour l'utilisateur ${id}`
                });
            }
        })
        .catch(e => {
            res.status(500).send({
                message: e.message || `Erreur de mise à jour de l'utilisateur ${id}`
           })
        })
}

// Delete an User
exports.delete = (req, res) => {
    const id = req.params.id;

    User.destroy({
        where: { id: id }
    })
        .then(num => {
            if (num === 1) {
                res.send({
                    message: 'Utilisateur supprimé avec succès'
                });
            } else {
                res.send({
                    message: `Peut pas supprimer l'utilisateur avec l'id ${id}`
                });
            }
        })
        .catch(e => {
            res.status(500).send({
                message: e.message || 'Peut pas supprimer l\'utilisateur avec l\'id' + id
            });
        });
};

// Delete all Users from the database.
exports.deleteAll = (req, res) => {
    User.destroy({
        where: {},
        truncate: false
    })
        .then(nums => {
            res.send({ message : `${nums} Utilisateurs supprimés avec succès.` });
        })
        .catch(e => {
            message:
                e.message || 'Erreur lors de la suppression de tous les utilisateur'
        })
};

// Find all published Users
exports.findAllPublished = (req, res) => {
    User.findAll({
        where: {}
    })
        .then(data => {
            res.send(data);
        })
        .catch(e => {
            res.status(500).send({
                message: e.message || `Erreur get utilisateurs`
            })
        })
};
