const express = require('express');
var app = express();
const users = require('../controller/user.controller.js');
var router = require('express').Router()

// Create
router.post('/', users.create());

// GetAll
router.get('/', users.findAll());

// GetOne
router.get('/:id', users.findOne());

// Update
router.put('/:id', users.update());

// Delete
router.delete('/:id', users.delete());

app.use('/api/users', router);

module.exports = router;
