const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();

var corsOptions = {
    origin : "http://localhost:8086"
};

app.use(cors(corsOptions));

// Parse request de type json
app.use(bodyParser.json());

app.use(bodyParser.urlencoded({ extended : true }));

// Route
app.get('/', (req, res) => {
    res.json({ message : "Welcome" });
});
/*
// Require routes
require('./app/routes/users.routes')(app);*/
// Setting ports
const PORT = process.env.PORT || 8080;

app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});

const db = require('./app/models');

// Dev mode
/*db.sequelize.sync({ force: true }).then(() => {
    console.log("Drop and re-sync db.");
});*/

db.sequelize.sync();
